﻿$(document).ready(function () {

    $('#contactForm').validate({
        rules: {
            name: {
                required: true,
                email: false
            },
            email: {
                required: true,
                email: true                
            },
            phone: {
                required: false,
                email: false,
                minlength: 10
            },
            body: {
                required: true,
                email: false                
            }
        },
        messages: {
            name: "You have a great name. Let's see it!",
            email: "We'll keep your email a secret.",            
            phone: "We need at least 10 digits. 'Merica.",
            body: "We'd love to hear your thoughts!"
        },
        showErrors: function (errorMap, errorList) {            
            $.each(this.validElements(), function (index, element) {
                var $element = $(element);
                $element.data("title", "")
                    .removeClass("error")
                    .tooltip("destroy");
            });            
            $.each(errorList, function (index, error) {
                var $element = $(error.element);
                $element.tooltip("destroy")
                    .data("title", error.message)
                    .addClass("error")
                    .tooltip().addClass("test");                
            });
        },
        submitHandler: function () {
            var name = $("#name").val();
            var email = $("#email").val();
            var phone = $("#phone").val();
            var body = $("#body").val();

            var url = "/api/Contact/SendEmail"

            $.ajax({
                type: "POST",
                url: url,
                data: JSON.stringify({ 'Name': name, 'Email': email, 'Phone': phone, 'Body': body }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: fnsuccesscallback,
                error: fnerrorcallback
            });
        }
    });

});

function fnsuccesscallback(data) {
    var name = $("#name").val("");
    var email = $("#email").val("");
    var phone = $("#phone").val("");
    var body = $("#body").val("");
    $('#modalMessageSuccess').modal('show');
};
function fnerrorcallback(result) {
    $('#modalMessageFailure').modal('show');
};