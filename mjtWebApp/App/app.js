﻿define([
    'kendo',
    'views/layout/layout',
    'views/home/home',
    'views/solutions/solutions',
    'views/about/about',
    'sendEmail'
], function (kendo, layout, home, solutions, about, sendEmail) {

    // the application router
    var router = new kendo.Router({
        init: function () {
        
            // render the layout first
            layout.render("#applicationHost");
        },
        routeMissing: function (e) {
        
            // debug shim writes console errors to the browser dev tools
            debug.error('No Route Found', e.url);
        },
        
        change: function (e) {
            
            // publish an event whenever the route changes
            $.publish('/router/change', [e]);
        }
    });

    // Add new routes here...
    
    router.route('/', function (e) {
        layout.showIn("#content", home);
    });

    router.route('/solutions', function (e) {
        layout.showIn("#content", solutions);
    });

    router.route('/about', function (e) {
        layout.showIn("#content", about);
    });

    return router;

});