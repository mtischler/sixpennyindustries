﻿define([
    'text!views/solutions/solutions.html'
], function (solutionsTemplate) {

    var viewModel = kendo.observable({
        title: 'Let\'\s Work Together'
    });

    var view = new kendo.View(solutionsTemplate, {
        model: viewModel,
        show: function (e) {
            kendo.fx(this.element).fade('in').duration(500).play();
        }
    });

    return view;

});
