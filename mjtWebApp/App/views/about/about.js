﻿define([
    'text!views/about/about.html'
], function (aboutTemplate) {

    var viewModel = kendo.observable({
        title: 'Us'
    });

    var view = new kendo.View(aboutTemplate, {
        model: viewModel,
        show: function (e) {
            kendo.fx(this.element).fade('in').duration(500).play();
        }
    });

    return view;

});