﻿using System;
using System.Web.Optimization;

namespace mjtWebApp
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            AddDefaultIgnorePatterns(bundles.IgnoreList);

            bundles.Add(
              new ScriptBundle("~/scripts/vendor")
                .Include("~/scripts/jquery-2.1.4.min.js")
                .Include("~/scripts/jquery.validate.min.js")
                .Include("~/scripts/pubsub.js")
                .Include("~/scripts/bootstrap.js")
                .Include("~/scripts/debug.js")
              );

            bundles.Add(
              new StyleBundle("~/Content/css")
                .Include("~/Content/kendo/2015.3.1111/kendo.common-bootstrap.min.css")
                .Include("~/Content/kendo/2015.3.1111/kendo.bootstrap.min.css")
                .Include("~/Content/kendo/2015.3.1111/kendo.bootstrap.mobile.min.css")
                .Include("~/Content/bootstrap.css")
                .Include("~/Content/font-awesome.css")
                .Include("~/Content/css/*.css")
              );
        }

        public static void AddDefaultIgnorePatterns(IgnoreList ignoreList)
        {
            if (ignoreList == null)
            {
                throw new ArgumentNullException("ignoreList");
            }

            ignoreList.Ignore("*.intellisense.js");
            ignoreList.Ignore("*-vsdoc.js");

            //ignoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
            //ignoreList.Ignore("*.min.js", OptimizationMode.WhenDisabled);
            //ignoreList.Ignore("*.min.css", OptimizationMode.WhenDisabled);
        }
    }
}