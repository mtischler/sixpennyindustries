﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using mjtWebApp.Models;
using System.Web.Http;

namespace mjtWebApp.Controllers
{
    public class ContactController : ApiController
    {
        public ContactController()
        {
        }

        // POST /api/Contact/SendEmail
        [HttpPost]
        public void SendEmail(ContactViewModel message)
        {            
            if (ModelState.IsValid)
            {
                try
                {                    
                    message.SentDate = DateTime.Now;

                    message.IP = System.Web.HttpContext.Current.Request.UserHostAddress;

                    SmtpClient smtpClient = new SmtpClient();

                    MailMessage m = new MailMessage(

                        "terminus@mylawsmith.com", // From

                        "mjtischler@gmail.com", // To

                        "A message from SixPenny.com", // Subject

                        message.BuildMessage()); // Body              

                    smtpClient.Send(m);
                }

                catch (HttpException e)
                {
                    //TODO: Log the exception
                }                
            }

        }
    }
}