# README #

I built this Single Page Application (SPA) as a public-facing advertisement for my services and as portfolio piece. The deployed version of this project is located at ...TODO.

### Screen grabs ###

* Welcome Page

![Alt text](https://bytebucket.org/mtischler/sixpennyindustries/raw/052092e69961b1c6fb00de6a1871074cd4507307/mjtWebApp/Content/images/homePageScreenshot.jpg "Welcome Page")

* About/Contact Page

![Alt text](https://bytebucket.org/mtischler/sixpennyindustries/raw/d1e9b32ae87001aaf33fe99ca19aadaaa719aa45/mjtWebApp/Content/images/aboutPageScreenshot.jpg "About/Contact Page")

### What is this repository for? ###

* This SPA provides an advertorial web presence my development house. All images and copy are my own, except the logo which was created based on a design by [Graphics Springs](https://www.graphicsprings.com).
* 0.8 (MVP version 1)
* Technologies:
	* Web frameworks: [ASP.NET MVC 5](http://www.asp.net/mvc/mvc5)
	* Front-end framework: [Bootstrap](https://getbootstrap.com), [Kendo UI Core](http://www.telerik.com/kendo-ui/open-source-core)
	* Javascript libraries: [jQuery](https://jquery.com/), [RequireJS](http://requirejs.org/)

### Much Obliged ###

* This is my second public solo project since forming [our company](https://www.mylawsmith.com) in 2014, but I'm forever indebted to my ILS partners for the tutelage, patience, and faith they've always provided me.
* Many thanks to the open source community, whose continued contributions not only show the value of free information but truly prove the inherent goodness of mankind.
* I have hundreds, if not thousands, of heroes over at [Stack Overflow](https://stackoverflow.com). Thank you for lending an altruistic hand to lurkers like me; I'll repay the debt once I finally "see the matrix."

### My Story ###

I sought to design a site that:

* Loaded fast
* Was responsive
* Had a secure connection
* Would appear near the top of a Google search for similar services
* Used copious amounts of the color mauve

Next, I determined which technologies to use:

* Architectural pattern: MVC, MVVM, or MVW
	* I intend for this webapp to have several database features, including user comments on a future blog. Given my earlier experience with MVC, and a desire to get back to working in that pattern, I decided to go with it for this project.
* Web framework: ASP.NET or KnockoutJS
	* While I had been working recently with KnockoutJS and AngularJS, I really wanted to hone my backend skills in C# and decided to roll with ASP.NET.
	* UPDATE: As a result of bringing in RouteDebugger for the Web API, I also added KnockoutJS... I'm not sure yet how I'm going to use it to enhance the project, but I'm glad it's there.
* Frond-end framework: Bootstrap, Foundation, Zimit, or InK
	* This was easy: Bootstrap. I love it and know it, but I'm interested in learning more about the alternatives for future projects.
* Deployment server: SmarterASP.NET, Arvixe, Azure, AWS, HostGator
	* TODO
	* For reference, I've set up deployments on all of these hosts. I would rate them 1) AWS 2) Azure 3) SmarterASP.NET 4) HostGator 5) Arvixe.

After that, I began development (and copious overconsumption of coffee and spoonfuls of peanut butter).

### To do ###

* Improve Google search listing
	* Optimize for SEO
	* Add Google Analytics
	* Add 'always on' SSL connection
	* Make site mobile-friendly
* Improve page load performance		
	* Remove unused libraries
	* Bundle/minify CSS
	* Bundle/minify JS
	* ~~Move JS out of inline style and into viewmodel~~
		* I refactored the the sendEmail JS code into a separate sendEmail.js file, but this can be enhanced with a viewmodel.
* Add UI automation tests
* Add email contact form
* Add blog
	* Add comment system

### How do I get set up? ###

* Download the [Kendo UI Spa Template](https://visualstudiogallery.msdn.microsoft.com/5af151b2-9ed2-4809-bfe8-27566bfe7d83)
* Clone the Git repository
* ~~Adjust VS project properties to [allow IIS Express to create a localhost SSL connection](https://www.hanselman.com/blog/WorkingWithSSLAtDevelopmentTimeIsEasierWithIISExpress.aspx)~~
* To ensure proper client-side debugging, start the app in the 'Debug' configuration. This will prevent bundling/minifying of the scripts and styles.

### Contribution guidelines ###

* Please code review this project, write tests, and/or provide constructive/destructive criticism.
* Feel free to fork this repo and do with it what you will, with the exception of site copy, images, and branding, which are property of Sixpenny Industries LLC (unless otherwise noted).

### Who do I talk to? ###

* [Matt Tischler](mailto:matt@mylawsmith.com)

### Image credits ###

* [Rubicon](https://commons.wikimedia.org/wiki/File:Savignano_sul_Rubicone_-_Ponte_romano_innevato_(febbraio_2012).jpg)
* [Workhole](https://commons.wikimedia.org/wiki/File:Photographing_a_wormhole.jpg)
* [Millennium Falcon](https://commons.wikimedia.org/wiki/File:Millennium_Falcon_in_LEGO.jpg)